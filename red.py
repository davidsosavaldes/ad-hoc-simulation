from sys import argv
from random import randint
from math import sqrt

class Nodes(object):
	def __init__(self, max_nodes):
		self.max_nodes = max_nodes
		self.node_messages_to_send = list()
		self.node_messages = list()
		self.set_size_of_messages()

	# Creando las posiciones de los nodos
	def create_node_position(self, x=100, y=100):
		self.nodes = list()
		for i in range(self.max_nodes):
			rx, ry = randint(0,x),randint(0,y)
			self.nodes.append((rx,ry))

	# Calculando la distancia entre dos nodos
	def eucledian_distance(self,coord1,coord2):
		x1,y1 = coord1
		x2,y2 = coord2
		return round(sqrt(((x2-x1)**2)+((y2-y1)**2)),2)

	# Por cada nodo creado verificar si tiene un vecino dependiendo de un umbral dado
	def check_if_neighbor(self,umbral):
		self.neighbors = dict()
		for i in range(len(self.nodes)):
			n = list()
			# Enumerar los nodos para tener un indice y que no se hagan muchos datos
			for num,node in enumerate(self.nodes):
				# Es vecino y no es el mismo nodo?
				if((self.eucledian_distance(self.nodes[i],node)<=umbral)and(self.nodes[i]!=node)):
					n.append(num)
			self.neighbors[i] = n
			# Poner los mensajes en la cola, i representa el indice del nodo
			id_sender = i
			self.queue_node_messages_to_send(i,self.neighbors[i],id_sender)

	def queue_node_messages_to_send(self,node, neighbors_of_node, id_sender):
		queue_messages = list()
		count = 0
		for neighbor in neighbors_of_node:
			# agregando el id unico a cada mensaje
			id_message = "%d%d"%(id_sender,count)
			# poner en la cola un mensaje binario y agregar el id
			queue_messages.append(("".join([str(randint(0,1)) for i in range(10)])+id_message,neighbor))
			count+=1
		self.node_messages_to_send[node] = queue_messages

	def set_size_of_messages(self):
		for i in range(self.max_nodes):
			self.node_messages.append("")
			self.node_messages_to_send.append("")

	def send_messages(self):
		#while in self.node_messages_to_send:
		for i in range(len(self.nodes)):
			for message, node_recv in self.node_messages_to_send[i]:
				print message,node_recv, "Mensaje enviado"


def main():
	insnode = Nodes(int(argv[1]))
	insnode.create_node_position()
	insnode.check_if_neighbor(10)

	# Debug
	print "Nodos creados:", insnode.nodes, "\n"
	for i in range(len(insnode.nodes)):

		print "Nodo:",i,"Pos:", insnode.nodes[i]
		print "Vecinos: ", insnode.neighbors[i]
		print "Mensajes en cola: ",insnode.node_messages_to_send[i]
		print 
	#print insnode.nodes[0],insnode.nodes[1],insnode.eucledian_distance(insnode.nodes[0],insnode.nodes[1])

	insnode.send_messages()
main()




