from sys import argv, exit
from random import randint, uniform
from math import sqrt
import pygame
from pygame.locals import *

#Some global colors for pygame
WHITE = (255, 255, 255)
BLUE = (0, 0,255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0,0,0)
BROWN = (220,122,24)

class Nodo(object):
	def __init__(self, dimention, id_node, radius, life):
		self.id = id_node
		self.messages = list()
		self.messages_send = list()
		self.radius = radius
		self.umbral = self.radius*1.75
		self.life = life
		self.set_position(dimention)

	def set_position(self, max_dimention):
		max_x, max_y = max_dimention
		x,y = randint(0, max_x-self.radius), randint(0, max_y-self.radius)
		self.position = (x,y)

	def eucledian_distance(self,coord1,coord2):
		x1,y1 = coord1
		x2,y2 = coord2
		return sqrt(((x2-x1)**2)+((y2-y1)**2))

	def verify_neighbors(self, all_nodes):
		self.neighbors = list()
		for num,node in enumerate(all_nodes):
			if ((self.eucledian_distance(self.position, node) <= self.umbral) and (self.position!=node)):
				self.neighbors.append(num)

	def queue_messages_to_send(self):
		for num,neighbor_id in enumerate(self.neighbors):
			unique_id = "_%d_%d%d_%d"%(self.id,num, randint(0,100),neighbor_id)
			message = "".join([str(randint(0,1)) for i in range(8)])+unique_id
			self.messages_send.append(message)

	# def send_message(self,node_output):
	#no jala
	# 	if self.messages_send:
	# 		value_message = self.messages_send[-1]
	# 		data, remiter, value_id, node_r_id = value_message.split("_")
	# 		node_output.messages.append((value_message,remiter))#+"_"+str(node_id))
	# 		del self.messages_send[-1]

	def draw_node_and_signal(self, DISPLAYSURF,FONT):
		try:
			radius_life = int((self.life*self.radius)/100)
			pygame.draw.circle(DISPLAYSURF,GREEN,self.position, radius_life, 0)
		except:
			#print "Hay un radio negativo:",radius_life, "en", self.id
			pygame.draw.circle(DISPLAYSURF,GREEN,self.position, 0, 0)
		pygame.draw.circle(DISPLAYSURF, BLUE, self.position, 5, 1)
		pygame.draw.circle(DISPLAYSURF,RED, self.position, self.radius ,1)
		text_surface = FONT.render('%d'%(self.id),1, BLACK)
		DISPLAYSURF.blit(text_surface,self.position)
		pygame.draw.rect

class Board(object):
	def __init__(self, amount_nodes=100, dimention = (200,200)):
		self.dimention = dimention
		self.amount_nodes = amount_nodes
		pygame.init()
		self.FONT = pygame.font.Font(None,18)
		self.DISPLAYSURF = pygame.display.set_mode(self.dimention)
		pygame.display.set_caption('Examen de redes')
		self.create_nodes()
		self.create_nodes_instances()

	def create_nodes(self):
		self.nodes = list()
		for unique_id in range(self.amount_nodes):
			self.nodes.append(Nodo(self.dimention, unique_id, 50, 100))
		self.nodes_coords = [node.position for node in self.nodes]

	def create_nodes_instances(self):
		for node in self.nodes:
			node.verify_neighbors(self.nodes_coords)
			node.queue_messages_to_send()

	def send_messages_of_node(self, node_id):
		if self.nodes[node_id].messages_send:
			for i in range(len(self.nodes[node_id].messages_send)):
				value_message = self.nodes[node_id].messages_send[-1]
				data,remiter,value_id,node_r_id = value_message.split("_")
				self.nodes[int(node_r_id)].messages.append((value_message,remiter))#+"_"+str(node_id))
				del self.nodes[node_id].messages_send[-1]

	def set_animation(self):
		fpsClock = pygame.time.Clock()
		while True:
			self.DISPLAYSURF.fill(WHITE)
			for node in self.nodes:
				if node.life > 0:
					node.life = node.life-uniform(0,5)
					node.draw_node_and_signal(self.DISPLAYSURF,self.FONT)
				if node.life <= 0:
					node.life = 100
					node.set_position(self.dimention)
			for event in pygame.event.get():
				if event.type == QUIT:
					pygame.quit()
					exit()
			pygame.display.update()
			fpsClock.tick(10)

game = Board(int(argv[1]),(800,500))

for node in game.nodes:
	#node.draw_node_and_signal(game.DISPLAYSURF,game.FONT)
	print "Node[%d]:"%(node.id)
	print "\tposition: %s \n\tneighbors: %s"%(node.position, node.neighbors)
	print "\tMessages to send: %s"%(node.messages_send)
	print "\tSend the messages...",
	game.send_messages_of_node(node.id)
	print "messages sended!!"#, node.messages_send
print "\nVamos a revisar los mensajes de cada nodo"
for node in game.nodes:
	print "Nodo:[%d]\n\tInbox: %s"%(node.id,node.messages)
	#print "\tMessages to send:",node.messages_send

game.set_animation()
